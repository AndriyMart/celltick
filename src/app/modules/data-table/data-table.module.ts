import { DxPivotGridModule } from "devextreme-angular";
import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";

import { DataTableComponent } from "./components/data-table.component";

@NgModule({
    declarations: [
        DataTableComponent,
    ],
    imports: [
        DxPivotGridModule,
        CommonModule
    ],
    exports: [
        DataTableComponent,
    ],
})
export class DataTableModule { }
