import { Component } from "@angular/core";
import DevExpress from "devextreme";
import PivotGridDataSourceOptions = DevExpress.data.PivotGridDataSourceOptions;

import { KpiHttpService } from "../../../shared/http-services/kpi-http.service";
import { KpiItem } from "../../../shared/http-services/modules/kpi-item.module";
import { DateTimeHelper } from "../../../shared/helpers/date-time-helper";

@Component({
    selector: "app-data-table",
    templateUrl: "./data-table.component.html",
    styleUrls: [ "./data-table.component.scss" ],
})
export class DataTableComponent {

    public dataSource: PivotGridDataSourceOptions;

    constructor(
        private kpiHttpService: KpiHttpService,
    ) {
        this.loadData();
    }

    private loadData()
    : void {
        this.kpiHttpService.getAll()
            .subscribe(res => this.updateDataSource(res));
    }

    private updateDataSource(data: KpiItem[])
    : void {
        this.dataSource = {
            fields: [
                {
                    caption: "Operator",
                    width: 120,
                    dataField: "operator",
                    area: "row",
                },
                {
                    area: "column",
                    selector: (value: KpiItem) => DateTimeHelper.FormatDate(new Date(value.time)),
                },
                {
                    caption: "Status",
                    dataField: "status",
                    area: "column"
                },
                {
                    caption: "Count",
                    dataField: "count",
                    area: "data",
                    dataType: "string",
                    summaryType: "sum"
                },
            ],
            store: data,
        };
    }
}
