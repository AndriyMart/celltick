import { NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";
import { HttpClientModule } from "@angular/common/http";

import { AppComponent } from "./app.component";
import { DataTableModule } from "./modules/data-table/data-table.module";

@NgModule({
    declarations: [
        AppComponent
    ],
    imports: [
        HttpClientModule,
        BrowserModule,
        DataTableModule,
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule { }
