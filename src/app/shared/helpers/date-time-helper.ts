import * as moment from "moment";

export class DateTimeHelper {

    public static FormatDate(value: Date)
    : string {
        return moment(value).format("DD/MM/YYYY");
    }
}
