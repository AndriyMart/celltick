export class KpiItem {

    public count: number;
    public operator: string;
    public status: string;
    public time: number;

    constructor(init?: Partial<KpiItem>) {
        Object.assign(this, init);
    }
}
