import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";

import { KpiItem } from "./modules/kpi-item.module";

@Injectable({
    providedIn: "root"
})
export class KpiHttpService {

    constructor(
        private http: HttpClient,
    ) {}

    public getAll()
    : Observable<KpiItem[]> {
        return this.http.get("../../../assets/docs/kpi.json") as Observable<KpiItem[]>;
    }
}
